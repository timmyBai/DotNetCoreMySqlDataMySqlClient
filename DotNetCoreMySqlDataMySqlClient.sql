CREATE DATABASE IF NOT EXISTS `DotNetCoreMySqlDataMySqlClient`;
USE `DotNetCoreMySqlDataMySqlClient`;

CREATE TABLE IF NOT EXISTS `student_basic` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `gender` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) COMMENT='學生基本資料';
