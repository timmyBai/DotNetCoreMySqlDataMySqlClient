using DotNetCoreMySqlDataMySqlClient.Tests.Utils;

public class Program
{
    private static DotNetCoreMySqlConnection conn = null;

    public static void Main()
    { 
        try
        {
            using (conn = new DotNetCoreMySqlConnection())
            {
                conn.Connect();

                Console.WriteLine(conn.State());
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.ToString());
        }
        finally
        {
            conn.Dispose();
            conn.Close();
        }

        Console.ReadKey();
    }
}