namespace DotNetCoreMySqlDataMySqlClient.Utils
{
    public class DotNetCoreMySqlSetting
    {
        /// <summary>
        /// 主機位置
        /// </summary>
        public string Host = "127.0.0.1";

        /// <summary>
        /// 使用者名稱
        /// </summary>
        public string User = "root";

        /// <summary>
        /// 密碼
        /// </summary>
        public string Password = "123456";

        /// <summary>
        /// 資料庫名稱
        /// </summary>
        public string DataBase = "DotNetCoreMySqlDataMySqlClient";

        /// <summary>
        /// 連線 port 號
        /// </summary>
        public string Port = "3306";

        /// <summary>
        /// 啟用 ssl 連線
        /// </summary>
        public string SSLMode = "none";

        /// <summary>
        /// 啟用公鑰連線
        /// </summary>
        public string AllowPublicKeyRetrieval = "false";

        /// <summary>
        /// 啟用 mysql 變數
        /// </summary>
        public string AllowUserVariables = "true";

        /// <summary>
        /// 連線逾時(秒)
        /// </summary>
        public int ConnectionTimeout = 3;

        public string GetConnectionString()
        {
            return string.Format(
                "Server={0}; Port={1}; User ID={2}; Password={3}; Database={4}; SslMode={5}; AllowPublicKeyRetrieval={6}; AllowUserVariables={7}; Connection Timeout={8}",
                Host,
                Port,
                User,
                Password,
                DataBase,
                SSLMode,
                AllowPublicKeyRetrieval,
                AllowUserVariables,
                ConnectionTimeout
            );
        }
    }
}
