using DotNetCoreMySqlDataMySqlClient.Tests.Models;
using DotNetCoreMySqlDataMySqlClient.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MySql.Data.MySqlClient;
using System.Data;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace DotNetCoreMySqlDataMySqlClient.Tests.Tests.Unit
{
    [TestClass]
    public class DeleteTests
    {
        /// <summary>
        /// 測試資料庫刪除資料(Dapper)
        /// </summary>
        [Test]
        [TestMethod]
        public void TestMysqlConnectionDeleteOnlyDataForDapper()
        {
            using (DotNetCoreMySqlConnection conn = new DotNetCoreMySqlConnection())
            {
                conn.Connect();
                conn.BeginTransaction();

                string sqlStr = "SELECT * FROM student_basic LIMIT 3";

                var result = conn.Query<StudentModels>(sqlStr);

                sqlStr = "DELETE FROM student_basic WHERE id = @id";

                var deleteRow = conn.Execute(sqlStr, new
                {
                    id = result.ToList()[0].id
                });

                conn.Commit();
                conn.Dispose();

                Assert.IsTrue(deleteRow == 1);
                Assert.IsTrue(conn.RowsCount == 1);
            }
        }

        /// <summary>
        /// 測試資料庫刪除資料(Dapper) Async 方法
        /// </summary>
        [Test]
        [TestMethod]
        public async Task TestMysqlConnectionDeleteOnlyDataForDapperAsync()
        {
            using (DotNetCoreMySqlConnection conn = new DotNetCoreMySqlConnection())
            {
                await conn.ConnectAsync();
                await conn.BeginTransactionAsync();

                string sqlStr = "SELECT * FROM student_basic LIMIT 3";

                var result = await conn.QueryAsync<StudentModels>(sqlStr);

                sqlStr = "DELETE FROM student_basic WHERE id = @id";

                var deleteRow = await conn.ExecuteAsync(sqlStr, new
                {
                    id = result.ToList()[0].id
                });

                await conn.CommitAsync();
                await conn.DisposeAsync();

                Assert.IsTrue(deleteRow == 1);
                Assert.IsTrue(conn.RowsCount == 1);
            }
        }

        /// <summary>
        /// 測試資料庫刪除資料(MysqlCommand)
        /// </summary>
        [Test]
        [TestMethod]
        public void TestMysqlConnectionDeleteOnlyDataMysqlCommand()
        {
            using (DotNetCoreMySqlConnection conn = new DotNetCoreMySqlConnection())
            {
                conn.Connect();
                conn.BeginTransaction();

                string sqlStr = "SELECT * FROM student_basic LIMIT 3";

                DataTable result = conn.Query(sqlStr);

                int id = Convert.ToInt32(result.Rows[0]["id"]);

                sqlStr = "DELETE FROM student_basic WHERE id = @id";

                var deleteRow = conn.Execute(sqlStr, new MySqlParameter("@id", id));

                conn.Commit();
                conn.Dispose();

                Assert.IsTrue(deleteRow == 1);
                Assert.IsTrue(conn.RowsCount == 1);
            }
        }

        /// <summary>
        /// 測試資料庫刪除資料(MysqlCommand)
        /// </summary>
        [Test]
        [TestMethod]
        public async Task TestMysqlConnectionDeleteOnlyDataMysqlCommandAsync()
        {
            using (DotNetCoreMySqlConnection conn = new DotNetCoreMySqlConnection())
            {
                await conn.ConnectAsync();
                await conn.BeginTransactionAsync();

                string sqlStr = "SELECT * FROM student_basic LIMIT 3";

                DataTable result = await conn.QueryAsync(sqlStr);

                int id = Convert.ToInt32(result.Rows[0]["id"]);

                sqlStr = "DELETE FROM student_basic WHERE id = @id";

                var deleteRow = await conn.ExecuteAsync(sqlStr, new MySqlParameter("@id", id));

                await conn.CommitAsync();
                await conn.DisposeAsync();

                Assert.IsTrue(deleteRow == 1);
                Assert.IsTrue(conn.RowsCount == 1);
            }
        }

        /// <summary>
        /// 測試生成 Delete Sql 語法
        /// </summary>
        [Test]
        [TestMethod]
        public async Task TestMysqlConnectionGenerateDeleteSql()
        {
            using (DotNetCoreMySqlConnection conn = new DotNetCoreMySqlConnection())
            {
                await conn.ConnectAsync();
                await conn.BeginTransactionAsync();

                string sqlStr = "SELECT * FROM student_basic LIMIT 3";

                var result = await conn.QueryAsync<StudentModels>(sqlStr);

                var param = new
                {
                    id = result.FirstOrDefault().id
                };

                conn.GenerateDeleteSql(out sqlStr, "student_basic", param);

                var deleteRow = await conn.ExecuteAsync(sqlStr, param);

                await conn.CommitAsync();
                await conn.DisposeAsync();

                Assert.IsTrue(deleteRow == 1);
                Assert.IsTrue(conn.RowsCount == 1);
            }
        }
    }
}
