using DotNetCoreMySqlDataMySqlClient.Tests.Models;
using DotNetCoreMySqlDataMySqlClient.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MySql.Data.MySqlClient;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace DotNetCoreMySqlDataMySqlClient.Tests.Tests.Unit
{
    public class InsertTests
    {
        /// <summary>
        /// 測試資料庫新增資料(Dapper)
        /// </summary>
        [Test]
        [TestMethod]
        public void TestMysqlConnectionInsertOnlyDataForDapper()
        {
            using (DotNetCoreMySqlConnection conn = new DotNetCoreMySqlConnection())
            {
                conn.Connect();
                conn.BeginTransaction();

                string sqlStr = "INSERT INTO student_basic (name, gender) VALUES (@name, @gender)";

                var insertRow = conn.Execute(sqlStr, new
                {
                    name = "王測試",
                    gender = "女"
                });

                conn.Commit();
                conn.Dispose();

                Assert.IsTrue(insertRow == 1);
                Assert.IsTrue(conn.RowsCount == 1);
            }
        }

        /// <summary>
        /// 測試資料庫新增資料(Dapper) Async 方法
        /// </summary>
        [Test]
        [TestMethod]
        public async Task TestMysqlConnectionInsertOnlyDataForDapperAsync()
        {
            using (DotNetCoreMySqlConnection conn = new DotNetCoreMySqlConnection())
            {
                await conn.ConnectAsync();
                await conn.BeginTransactionAsync();

                string sqlStr = "INSERT INTO student_basic (name, gender) VALUES (@name, @gender)";

                var insertRow = await conn.ExecuteAsync(sqlStr, new
                {
                    name = "王測試",
                    gender = "女"
                });

                await conn.CommitAsync();
                await conn.DisposeAsync();

                Assert.IsTrue(insertRow == 1);
                Assert.IsTrue(conn.RowsCount == 1);
            }
        }

        /// <summary>
        /// 測試資料庫新增資料(MysqlCommand)
        /// </summary>
        [Test]
        [TestMethod]
        public void TestMysqlConnectionInsertOnlyForMysqlCommand()
        {
            using (DotNetCoreMySqlConnection conn = new DotNetCoreMySqlConnection())
            {
                conn.Connect();
                conn.BeginTransaction();

                string sqlStr = "INSERT INTO student_basic (name, gender) VALUES (@name, @gender)";

                var insertRow = conn.Execute(sqlStr,
                    new MySqlParameter("@name", "方測試"),
                    new MySqlParameter("@gender", "男")
                );

                conn.Commit();
                conn.Dispose();

                Assert.IsTrue(insertRow == 1);
                Assert.IsTrue(conn.RowsCount == 1);
            }
        }

        /// <summary>
        /// 測試資料庫新增資料(MysqlCommand) Async 方法
        /// </summary>
        [Test]
        [TestMethod]
        public async Task TestMysqlConnectionInsertOnlyForMysqlCommandAsync()
        {
            using (DotNetCoreMySqlConnection conn = new DotNetCoreMySqlConnection())
            {
                await conn.ConnectAsync();
                await conn.BeginTransactionAsync();

                string sqlStr = "INSERT INTO student_basic (name, gender) VALUES (@name, @gender)";

                var insertRow = await conn.ExecuteAsync(sqlStr,
                    new MySqlParameter("@name", "方測試"),
                    new MySqlParameter("@gender", "男")
                );

                await conn.CommitAsync();
                await conn.DisposeAsync();

                Assert.IsTrue(insertRow == 1);
                Assert.IsTrue(conn.RowsCount == 1);
            }
        }

        /// <summary>
        /// 測試生成 Insert Sql 語法
        /// </summary>
        [Test]
        [TestMethod]
        public async Task TestMysqlConnectionGenerateInsertSql()
        {
            using (DotNetCoreMySqlConnection conn = new DotNetCoreMySqlConnection())
            {
                await conn.ConnectAsync();
                await conn.BeginTransactionAsync();

                string sqlStr = "";

                conn.GenerateInsertSql<StudentModels>(out sqlStr, "student_basic");

                var insertRow = await conn.ExecuteAsync(sqlStr, new
                {
                    gender = "男",
                    name = "方測試"
                });

                await conn.CommitAsync();
                await conn.DisposeAsync();

                Assert.IsTrue(insertRow == 1);
                Assert.IsTrue(conn.RowsCount == 1);
            }
        }
    }
}
