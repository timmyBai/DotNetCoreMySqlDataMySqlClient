using DotNetCoreMySqlDataMySqlClient.Tests.Models;
using DotNetCoreMySqlDataMySqlClient.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MySql.Data.MySqlClient;
using System.Data;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace DotNetCoreMySqlDataMySqlClient.Tests.Tests.Unit
{
    [TestClass]
    public class SelectTests
    {
        /// <summary>
        /// 測試資料庫取得資料(Dapper)
        /// </summary>
        [Test]
        [TestMethod]
        public void TestMysqlConnectionOnlyDataForDapper()
        {
            using (DotNetCoreMySqlConnection conn = new DotNetCoreMySqlConnection())
            {
                conn.Connect();

                string sqlStr = "SELECT * FROM student_basic LIMIT 3";

                var result = conn.Query<StudentModels>(sqlStr);

                Assert.IsTrue(result.Count() > 0);
            }
        }

        /// <summary>
        /// 測試資料庫取得資料(Dapper) Async 方法
        /// </summary>
        [Test]
        [TestMethod]
        public async Task TestMysqlConnectionOnlyDataForDapperAsync()
        {
            using (DotNetCoreMySqlConnection conn = new DotNetCoreMySqlConnection())
            {
                await conn.ConnectAsync();

                string sqlStr = "SELECT * FROM student_basic LIMIT 3";

                var result = conn.QueryAsync<StudentModels>(sqlStr).Result;

                Assert.IsTrue(result.Count() > 0);
            }
        }

        /// <summary>
        /// 測試資料庫取得資料(MysqlCommand)
        /// </summary>
        [Test]
        [TestMethod]
        public void TestMysqlConnectionGetOnlyDataForDataTable()
        {
            using (DotNetCoreMySqlConnection conn = new DotNetCoreMySqlConnection())
            {
                conn.Connect();

                string sqlStr = "SELECT * FROM student_basic WHERE name LIKE @name";

                DataTable result = conn.Query(sqlStr,
                    new MySqlParameter("@name", "王%")
                );

                conn.Dispose();

                Assert.IsTrue(result.Rows.Count > 0);
            }
        }

        /// <summary>
        /// 測試資料庫取得資料(MysqlCommand) Async 方法
        /// </summary>
        [Test]
        [TestMethod]
        public async Task TestMysqlConnectionGetOnlyDataForDataTableAsync()
        {
            using (DotNetCoreMySqlConnection conn = new DotNetCoreMySqlConnection())
            {
                await conn.ConnectAsync();

                string sqlStr = "SELECT * FROM student_basic WHERE name LIKE @name";

                DataTable result = await conn.QueryAsync(sqlStr,
                    new MySqlParameter("@name", "王%")
                );

                await conn.DisposeAsync();

                Assert.IsTrue(result.Rows.Count > 0);
            }
        }

        /// <summary>
        /// 測試生成 Select Sql 語法
        /// </summary>
        [Test]
        [TestMethod]
        public async Task TestMysqlConnectionGenerateSelectSql()
        {
            using (DotNetCoreMySqlConnection conn = new DotNetCoreMySqlConnection())
            {
                await conn.ConnectAsync();

                string sqlStr = "";

                var param = new
                {
                    name = "王測試"
                };

                conn.GenerateSelectSql<StudentModels>(out sqlStr, "student_basic", param);

                var result = await conn.QueryAsync<StudentModels>(sqlStr, param);

                Assert.IsTrue(result.Count() > 0);
            }
        }
    }
}
