using DotNetCoreMySqlDataMySqlClient.Tests.Models;
using DotNetCoreMySqlDataMySqlClient.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MySql.Data.MySqlClient;
using System.Data;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace DotNetCoreMySqlDataMySqlClient.Tests.Tests.Unit
{
    [TestClass]
    public class UpdateTests
    {
        /// <summary>
        /// 測試資料庫更新資料(Dapper)
        /// </summary>
        [Test]
        [TestMethod]
        public void TestMysqlConnectionUpdateOnlyDataForDapper()
        {
            using (DotNetCoreMySqlConnection conn = new DotNetCoreMySqlConnection())
            {
                conn.Connect();
                conn.BeginTransaction();

                string sqlStr = "SELECT * FROM student_basic LIMIT 3";

                var result = conn.Query<StudentModels>(sqlStr);

                sqlStr = "UPDATE student_basic SET gender = @gender WHERE id = @id";

                var updateRow = conn.Execute(sqlStr, new
                {
                    gender = "男",
                    id = result.FirstOrDefault().id
                });

                conn.Commit();
                conn.Dispose();

                Assert.IsTrue(updateRow == 1);
                Assert.IsTrue(conn.RowsCount == 1);
            }
        }

        /// <summary>
        /// 測試資料庫更新資料(Dapper) Async 方法
        /// </summary>
        [Test]
        [TestMethod]
        public async Task TestMysqlConnectionUpdateOnlyDataForDapperAsync()
        {
            using (DotNetCoreMySqlConnection conn = new DotNetCoreMySqlConnection())
            {
                await conn.ConnectAsync();
                await conn.BeginTransactionAsync();

                string sqlStr = "SELECT * FROM student_basic LIMIT 3";

                var result = await conn.QueryAsync<StudentModels>(sqlStr);

                sqlStr = "UPDATE student_basic SET gender = @gender WHERE id = @id";

                var updateRow = await conn.ExecuteAsync(sqlStr, new
                {
                    gender = "男",
                    id = result.FirstOrDefault().id
                });

                await conn.CommitAsync();
                await conn.DisposeAsync();

                Assert.IsTrue(updateRow == 1);
                Assert.IsTrue(conn.RowsCount == 1);
            }
        }

        /// <summary>
        /// 測試資料庫更新資料(MysqlCommand)
        /// </summary>
        [Test]
        [TestMethod]
        public void TestMysqlConnectionUpdateOnlyForMysqlCommand()
        {
            using (DotNetCoreMySqlConnection conn = new DotNetCoreMySqlConnection())
            {
                conn.Connect();
                conn.BeginTransaction();

                string sqlStr = "SELECT * FROM student_basic LIMIT 3";

                DataTable result = conn.Query(sqlStr);

                sqlStr = "UPDATE student_basic SET gender = @gender WHERE id = @id";

                var updateRow = conn.Execute(sqlStr,
                    new MySqlParameter("@gender", "男"),
                    new MySqlParameter("@id", result.Rows[0]["id"])
                );

                conn.Commit();
                conn.Dispose();

                Assert.IsTrue(updateRow == 1);
                Assert.IsTrue(conn.RowsCount == 1);
            }
        }

        /// <summary>
        /// 測試資料庫更新資料(MysqlCommand) Async 方法
        /// </summary>
        [Test]
        [TestMethod]
        public async Task TestMysqlConnectionUpdateOnlyForMysqlCommandAsync()
        {
            using (DotNetCoreMySqlConnection conn = new DotNetCoreMySqlConnection())
            {
                await conn.ConnectAsync();
                await conn.BeginTransactionAsync();

                string sqlStr = "SELECT * FROM student_basic LIMIT 3";

                DataTable result = await conn.QueryAsync(sqlStr);

                sqlStr = "UPDATE student_basic SET gender = @gender WHERE id = @id";

                var updateRow = await conn.ExecuteAsync(sqlStr,
                    new MySqlParameter("@gender", "男"),
                    new MySqlParameter("@id", result.Rows[0]["id"])
                );

                await conn.CommitAsync();
                await conn.DisposeAsync();

                Assert.IsTrue(updateRow == 1);
                Assert.IsTrue(conn.RowsCount == 1);
            }
        }

        /// <summary>
        /// 測試生成 Update Sql 語法
        /// </summary>
        [Test]
        [TestMethod]
        public async Task TestMysqlConnectionGenerateUpdateSqlPerformanceVersion()
        {
            using (DotNetCoreMySqlConnection conn = new DotNetCoreMySqlConnection())
            {
                await conn.ConnectAsync();
                await conn.BeginTransactionAsync();

                string sqlStr = "SELECT * FROM student_basic LIMIT 1";

                var result = await conn.QueryAsync<StudentModels>(sqlStr);

                UpdateStudentModels updateStudentModels = new UpdateStudentModels();
                updateStudentModels.name = "藍測試";
                updateStudentModels.gender = result.SingleOrDefault().gender == "男" ? "女" : "男";

                var param = new
                {
                    id = result.SingleOrDefault().id,
                    name = updateStudentModels.name,
                    gender = result.SingleOrDefault().gender
                };

                conn.GenerateUpdateSql(result, updateStudentModels, out sqlStr, "student_basic", new
                {
                    id = result.SingleOrDefault().id
                });

                var updateRow = await conn.ExecuteAsync(sqlStr, param);

                await conn.CommitAsync();
                await conn.DisposeAsync();

                Assert.IsTrue(updateRow == 1);
                Assert.IsTrue(conn.RowsCount == 1);
            }
        }
    }
}
