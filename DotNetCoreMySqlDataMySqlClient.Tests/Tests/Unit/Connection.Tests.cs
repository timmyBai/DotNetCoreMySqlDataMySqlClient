using DotNetCoreMySqlDataMySqlClient.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;
using System.Data;

namespace DotNetCoreMySqlDataMySqlClient.Tests.Tests.Unit
{
    [TestClass]
    public class ConnectionTests
    {
        /// <summary>
        /// 測試資料庫連線狀態
        /// </summary>
        [Test]
        [TestMethod]
        public void TestMysqlConnectionSuccess()
        {
            using (DotNetCoreMySqlConnection conn = new DotNetCoreMySqlConnection())
            {
                conn.Connect();

                Assert.IsTrue(conn.State() == true);
            }
        }

        /// <summary>
        /// 測試資料庫連線狀態 Async 方法
        /// </summary>
        /// <returns></returns>
        [Test]
        [TestMethod]
        public async Task TestMysqlConnectionSuccessAsync()
        {
            using(DotNetCoreMySqlConnection conn = new DotNetCoreMySqlConnection())
            {
                await conn.ConnectAsync();

                Assert.IsTrue(conn.StateAsync().Result == true);
            }
        }

        /// <summary>
        /// 測試新增資料庫連線狀態
        /// </summary>
        [Test]
        [TestMethod]
        public void TestMysqlCreateConnectSuccess()
        {
            using (DotNetCoreMySqlConnection conn = new DotNetCoreMySqlConnection())
            {
                var DotNetCoreMySqlDataMySqlClientDB = conn.CreateConnect();

                Assert.IsTrue(DotNetCoreMySqlDataMySqlClientDB.State == ConnectionState.Open);
            }
        }

        /// <summary>
        /// 測試新增資料庫連線狀態
        /// </summary>
        [Test]
        [TestMethod]
        public async Task TestMysqlCreateConnectionSuccessAsync()
        {
            using (DotNetCoreMySqlConnection conn = new DotNetCoreMySqlConnection())
            {
                var DotNetCoreMySqlDataMySqlClientDB = await conn.CreateConnectAsync();
                
                Assert.IsTrue(DotNetCoreMySqlDataMySqlClientDB.State == ConnectionState.Open);
            }
        }

        /// <summary>
        /// 測試資料庫停止連線與釋放資源
        /// </summary>
        [Test]
        [TestMethod]
        public void TestMysqlConnectionClose()
        {
            using (DotNetCoreMySqlConnection conn = new DotNetCoreMySqlConnection())
            {
                conn.Connect();
                conn.Close();
                conn.Dispose();

                Assert.IsTrue(conn.State() == false);
            }
        }

        /// <summary>
        /// 測試資料庫停止連線與釋放資源 Async 方法
        /// </summary>
        /// <returns></returns>
        [Test]
        [TestMethod]
        public async Task TestMysqlConnectionCloseAsync()
        {
            using (DotNetCoreMySqlConnection conn = new DotNetCoreMySqlConnection())
            {
                await conn.ConnectAsync();
                await conn.CloseAsync();
                await conn.DisposeAsync();

                Assert.IsTrue(conn.StateAsync().Result == false);
            }
        }

        /// <summary>
        /// 測試連線指定資料庫
        /// </summary>
        [Test]
        [TestMethod]
        public void TestMysqlConnectionDatabase()
        {
            using (DotNetCoreMySqlConnection conn = new DotNetCoreMySqlConnection())
            {
                conn.Connect("test_db");

                Assert.IsTrue(conn.State() == true);
            }
        }

        /// <summary>
        /// 測試連線指定資料庫 Async 方法
        /// </summary>
        [Test]
        [TestMethod]
        public async Task TestMysqlConnectionDatabaseAsync()
        {
            using (DotNetCoreMySqlConnection conn = new DotNetCoreMySqlConnection())
            {
                await conn.ConnectAsync("test_db");

                Assert.IsTrue(conn.StateAsync().Result == true);
            }
        }
    }
}
