﻿namespace DotNetCoreMySqlDataMySqlClient.Tests.Models
{
    /// <summary>
    /// 取得學員資料模型
    /// </summary>
    public class StudentModels
    {
        /// <summary>
        /// id
        /// </summary>
        public int id { get; set; } = 0;
        
        /// <summary>
        /// 名字
        /// </summary>
        public string name { get; set; } = "";
        
        /// <summary>
        /// 性別
        /// </summary>
        public string gender { get; set; } = "";
    }

    /// <summary>
    /// 更新學員資料模型
    /// </summary>
    public class UpdateStudentModels
    {
        /// <summary>
        /// 名字
        /// </summary>
        public string name { get; set; } = "";

        /// <summary>
        /// 性別
        /// </summary>
        public string gender { get; set; } = "";
    }
}
